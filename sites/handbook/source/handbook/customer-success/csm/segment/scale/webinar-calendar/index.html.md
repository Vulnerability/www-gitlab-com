---
layout: handbook-page-toc
title: "CSM/CSE Webinar Calendar"
---
## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

View the [CSM Handbook homepage](/handbook/customer-success/csm/) for additional CSM/CSE-related handbook pages.

Watch previously recorded webinars on our [YouTube Playlist](https://www.youtube.com/playlist?list=PL05JrBw4t0Kpczt4pRtyF147Uvn2bGGvq).

---

## Upcoming Webinars

We’d like to invite you to our free upcoming webinars during the month of October.

If you know someone who might be interested in attending, feel free to share the registration links with them. Everyone is welcome, and we hope to see you there!

### October 2022

### Intro to GitLab
#### October 5th, 2022 at 11:00AM-12:00PM Eastern Time/3:00-4:00 PM UTC

Are you new to GitLab? Join this webinar, where we will review what GitLab is, how it benefits you, and the recommended workflow to allow you to get the most out of the platform.
 
[<button class="btn btn-primary" type="button">Register</button>](https://gitlab.zoom.us/webinar/register/WN_BP6rcGcwRh6IERVmQpbkXA)

### Intro to GitLab Office Hours
#### October 5th, 2022 at 12:00PM-12:45PM Eastern Time/4:00-4:45 PM UTC

In our office hours following our Intro to GitLab webinar, we will focus on questions that are commonly asked by new users of GitLab. To help us prepare, please submit your questions as comments to [this issue](https://gitlab.com/gitlab-com/scale-cse-office-hours/-/issues/1). Our office hours are provided in a webinar format where we will address questions from our attendees submitted ahead of time via [the issue](https://gitlab.com/gitlab-com/scale-cse-office-hours/-/issues/1) and questions submitted live using the Zoom Q&A function

[<button class="btn btn-primary" type="button">Register</button>](https://gitlab.zoom.us/webinar/register/WN_-9NWyZpSQOCRhmmT9EaV7Q)

### Holistic Approach to Securing the Development Lifecycle
#### October 13th, 2022 at 11:00AM-12:00PM Eastern Time/3:00-4:00 PM UTC

This webinar will focus on fundamental practices for a secure software development lifecycle (SSDL) strategy, and how GitLab Ultimate helps you accelerate the end-to-end delivery of that SSDL strategy.
We will also address the security principle of shifting left and what that means when implementing the suite of security, compliance and audit capabilities available in GitLab Ultimate.
 
[<button class="btn btn-primary" type="button">Register</button>](https://gitlab.zoom.us/webinar/register/WN_6GLZk2LrTnypPFbY-4qeng)

### Intro to CI/CD
#### October 14th, 2022 at 11:00AM-12:00PM Eastern Time/3:00-4:00 PM UTC

Come learn about what CI/CD is and how it can benefit your team. We will cover an overview of CI/CD and what it looks like in GitLab. We will also cover how to get started with your first CI/CD pipeline in GitLab and the basics of GitLab Runners.
 
[<button class="btn btn-primary" type="button">Register</button>](https://gitlab.zoom.us/webinar/register/WN_PeY0d4XDRDq91m8bR4CW-A)

### Intro to CI/CD Office Hours
#### October 14th, 2022 at 12:00PM-12:45PM Eastern Time/4:00-4:45 PM UTC

Are you new to CI/CD? In our office hours following our Intro to CI/CD webinar, we will focus on questions that are commonly asked by new users of CI/CD with GitLab. To help us prepare, please submit your questions as comments to [this issue](https://gitlab.com/gitlab-com/scale-cse-office-hours/-/issues/2). Our office hours are provided in a webinar format where we will address questions from our attendees submitted ahead of time via [the issue](https://gitlab.com/gitlab-com/scale-cse-office-hours/-/issues/2) and questions submitted live using the Zoom Q&A function

[<button class="btn btn-primary" type="button">Register</button>](https://gitlab.zoom.us/webinar/register/WN_Z1mI48RETCy6ScYIGSP0GQ)

### Advanced CI/CD
#### October 18th, 2022 at 11:00AM-12:00PM Eastern Time/3:00-4:00 PM UTC

Expand your CI/CD knowledge while we cover advanced topics that will accelerate your efficiency using GitLab, such as pipelines, variables, rules, artifacts, and more. This session is intended for those who have used CI/CD in the past.
 
[<button class="btn btn-primary" type="button">Register</button>](https://gitlab.zoom.us/webinar/register/WN_sZYEaYg8S4i3cgkzi9MHuw)

### Advanced CI/CD Office Hours
#### October 18th, 2022 at 12:00PM-12:45PM Eastern Time/4:00-4:45 PM UTC

Are you beyond CI/CD fundamentals? In our office hours following our Advanced CI/CD webinar, we will focus on questions that typically arise for teams that have used CI/CD in the past. To help us prepare, please submit your questions as comments to [this issue](https://gitlab.com/gitlab-com/scale-cse-office-hours/-/issues/3). Our office hours are provided in a webinar format where we will address questions from our attendees submitted ahead of time via [the issue](https://gitlab.com/gitlab-com/scale-cse-office-hours/-/issues/3) and questions submitted live using the Zoom Q&A function

[<button class="btn btn-primary" type="button">Register</button>](https://gitlab.zoom.us/webinar/register/WN_NzyK7sTcTWanLNsVTcG9Iw)

### GitLab Adminstration on SaaS
#### October 20th, 2022 at 11:00AM-12:00PM Eastern Time/3:00-4:00 PM UTC

Learn how to manage groups, projects, permissions, and more as you embark on your journey as an Owner in GitLab SaaS. In this session, you will learn what you can and cannot control and customize on the SaaS platform, and come out a SME in administration.
 
[<button class="btn btn-primary" type="button">Register</button>](https://gitlab.zoom.us/webinar/register/WN_AOEEvVMQQW2jN4Ok89VUJQ)

### DevSecOps/Compliance
#### October 27th, 2022 at 11:00AM-12:00PM Eastern Time/3:00-4:00 PM UTC

GitLab enables developers and security to work together in a single tool, allowing for proactive security or “shifting left”. This session will cover what GitLab offers, how scan results integrate seamlessly with merge requests, and how to use the Security Dashboard to manage vulnerabilities.
 
[<button class="btn btn-primary" type="button">Register</button>](https://gitlab.zoom.us/webinar/register/WN_CPl3Yw1JRo6pKNrx8Ej8uw)

### DevSecOps/Compliance Office Hours
#### October 27th, 2022 at 12:00PM-12:45PM Eastern Time/4:00-4:45 PM UTC

Join us right after the DevSecOps/Compliance webinar for our office hours on the same topic. To help us prepare, please submit your questions as comments to [this issue](https://gitlab.com/gitlab-com/scale-cse-office-hours/-/issues/4). Our office hours are provided in a webinar format where we will address questions from our attendees submitted ahead of time via [the issue](https://gitlab.com/gitlab-com/scale-cse-office-hours/-/issues/4) and questions submitted live using the Zoom Q&A function

[<button class="btn btn-primary" type="button">Register</button>](https://gitlab.zoom.us/webinar/register/WN_e1otzrZoSZev9AC-QAOn3A)
