---
layout: handbook-page-toc
title: "Anti-Fraud Policy"
description: "GitLab's Anti-Fraud Policy"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}
