---
layout: markdown_page
title: "Development Director Shadow Program"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## What is the Development Director Shadow Program?

I got massive value out of participating in our [CEO Shadow Program](/handbook/ceo/shadow/) when [I did it in July of 2020](/blog/2020/07/08/ceo-shadow-impressions-takeaways/).

Since then, I have been thinking of doing something similar but smaller in scale. In July of 2021, I asked my teams if they would be interested in remotely shadowing me and several team members expressed interest as they wanted to learn what I do, how I do it, and how I make decisions.

## Benefits

### For the shadow

* Mentoring
* Learning opportunities
* Career development exploration

Blog on one shadow's experiences in the program: [The engineering director shadow experience at GitLab](/blog/2022/04/01/engineering-director-shadow/)

### For the engineering director

* Learning via reverse mentorship
* Feedback

## Criteria to be a shadow

1. I am in the Eastern US (GMT-4). The shadow must be available during some (but not all) of these working hours. Confirm this works for you before proceeding.
1. You have been with GitLab for at least one month.

## Process for requesting to be a shadow

1. Check the schedule below for availability.
1. Create an MR to add yourself to the table for the weeks in which you want to shadow via an MR. To get started, you can [edit the page here](https://gitlab.com/-/ide/project/gitlab-com/www-gitlab-com/edit/master/-/sites/handbook/source/handbook/engineering/development/shadow/director-shadow-program.html.md) (internal).
1. Obtain written approval from your manager that they are ok with you participating via your manager commenting in the MR that they approve, as your workload may need to be significantly reduced for the shadow program duration.
1. After your manager approves, assign the MR to me to review and merge.

## How does the shadowing work?

The engineering director shadow will have a chance to work alongside engineering director [Wayne Haber](https://gitlab.com/wayne) in the following ways:

* Attend all of scheduled meetings which are not marked as `private`, unless anyone in the meeting requests privacy (discussing compensation, etc.) You will be able to attend 1-1s, group meetings, and much more
* Review Documents, Issues, and Merge Requests linked to the meetings
* Take notes in the meeting documents in order to collect main points and outcomes
* Keep track of time and provide warnings allowing meeting participants to wrap up the meeting before the meeting end time comfortably
* Provide opinions during meetings with your thoughts and/or add your thoughts in the meeting notes

### Additional shadow program considerations

* The shadow will participate remotely.
* You can be a shadow for as little as one week or as much as two weeks in a six-month period.  The weeks do not need to be sequential.
* Review the roles of those with who Wayne will be meeting in order to have more context.
* If you don't have access to a document and the meeting participants are ok with allowing you to view it (which I will confirm), I will share my screen via Google Hangouts or grant you temporary access to the document.
* Feel free to ask me questions via Slack in `#wayne_shadow_program`, after a meeting concludes, via scheduling a meeting with me, or via an ad-hoc Zoom discussion.
* Review Wayne's [GitLab history](https://gitlab.com/wayne) and Slack messages I have written in public channels.
* Please let Wayne know if you notice him interrupting speakers, speaking too quickly, or not pausing often enough. These are things he is working on improving.
* Feel free to introduce yourself in a meeting when you feel this is appropriate (especially when there are only a few attendees and the meeting is not public inside GitLab). Tell participants who you are, what your regular job is, and that you are a shadow in this meeting.
* Even in meetings where you are unfamiliar with the subject matter, there is an opportunity to learn, document, and shape the evolution of GitLab's [values](/handbook/values/). 
* You may be asked for feedback on issues, merge requests, meeting notes, etc.

## Process for non-shadow meeting attendees

* If a meeting is public inside GitLab, shadows will attend the meeting as a regular participant
* Some meetings will discuss confidential information. In those cases, I will ask the meeting attendees if they are ok with a shadow being in the meeting (and if they are ok with the shadow having read or read/write access to the meeting notes document).

## What this program is not

It is not a performance evaluation or a step for a future promotion.

## Preparing for the program

1. Slack me in `#wayne_shadow_program` to let me know a couple of days before your first shadow
1. Schedule these coffee chats a couple of days before your first shadow:
    1. With me (Wayne), especially if we have not met previously
    1. With one of the previous shadows (See list below)
1. Plan to observe and ask questions
1. Reduce your workload by at least 75% during the shadowing time period. Don't plan to do your normal amount of usual work.
1. Commit to confidentiality. Participating in the shadow program is a privilege where you may be exposed to confidential information. This is underpinned by trust in the shadows to honor the confidentiality of topics being discussed and information shared. The continuation of this program is entirely dependent on shadows past, present, and future honoring this trust placed in them.
1. Review my [calendar](https://calendar.google.com/calendar/u/0?cid=d2hhYmVyQGdpdGxhYi5jb20).
1. Review my [readme](/handbook/engineering/readmes/wayne-haber/).
1. Join the slack channel `#sec-growth-datascience-people-leaders`.
1. Re-read GitLab's values prior to your shadow rotation, and be mindful of new and inventive ways that [CREDIT](/handbook/values/#credit) is lived out during the meetings you attend.
1. Confirm you have been added to the [wayne shadow program google group](https://groups.google.com/a/gitlab.com/g/wayne-shadow-program/members) so you will have access to private documents

## What to do after you participate

1. After your shadow completes, I would love to hear feedback on the program, what you learned, what you liked, what you didn't like, feedback on what I can do better, etc.
1. Consider sharing your learnings with your team and other peers via a blog, slack summary, etc.
1. Remove yourself from the [wayne shadow program google group](https://groups.google.com/a/gitlab.com/g/wayne-shadow-program/members) 

## Are other directors in engineering also allowing shadows?

No, not at this time.

## Schedule

A given week should be considered open for shadowing unless it is listed below.

| Week of | Shadow | Department | 
| ------ | ------ | ----- |
| 9/20/2021 | @warias | Marketing |
| 11/15/2021 | @mlindsay | Professional Serivces | 
| 12/13/2021 | @mlindsay | Professional Serivces | 
| 2/21/2022 | @oregand | Development |
| 2/28/2022 | @dmishunov | Development |
| 3/28/2022 | @rossfuhrman | Development |
| 4/25/2022 | @sam.figueroa | Development |
| 7/18/2022 | @bradleylee | Customer Success |
| 9/12/2022 | @fjdiaz | Marketing |

## Previous Shadow Blogs

1. [What I Learned as a Development Director](https://awkwardferny.medium.com/what-i-learned-as-an-engineering-director-shadow-at-gitlab-1a783cb564d0) - @fjdiaz
1. [The engineering director shadow experience at GitLab](https://about.gitlab.com/blog/2022/04/01/engineering-director-shadow/) - @warias
